Title: Debian won Linux Journal's Readers' Choice Award for Best Linux Distribution!
Date: 2018-02-14 23:00
Tags: debian, award
Slug: debian-linuxjournal-readers-choice-award
Author: Laura Arjona Reina
Status: published

Debian won [Linux Journal's Readers' Choice Award for Best Linux Distribution](http://www.linuxjournal.com/content/best-linux-distribution).

Thank you for all your support!

![Linux Journal Award](|static|/images/ljaward.png)
