Title: Great fonts in Debian 10 (or later)
Slug: great-fonts-debian-10
Date: 2020-06-09 13:00
Author: Gürkan Myczko
Artist: Gürkan Myczko
Tags: fonts, typography, artwork
Status: published

![An example of several fonts in Debian 10](|static|/images/fonts-debian.png)

Debian comes with tons of fonts for all kinds of purposes,
you can easily list them all (almost) with: `apt-cache search ^fonts-`

Above you can see a nice composition with examples of several fonts.
The composition is published under the
[MIT (Expat) license](https://www.debian.org/legal/licenses/mit)
and the source SVG (created with Inkscape) can be downloaded
[here](|static|/images/debian-fonts.svg).
You will need the fonts to be installed in your system so the SVG is correctly rendered.

If you want to learn more you can have a look at the wiki page about fonts
([https://wiki.debian.org/Fonts](https://wiki.debian.org/Fonts)),
and if you want to contribute or maintain fonts in Debian,
don't hesitate to join the [Fonts Team](https://wiki.debian.org/Teams/pkg-fonts)!
