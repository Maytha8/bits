Title: Nous desenvolupadors i mantenidors de Debian (maig i juny del 2020)
Slug: new-developers-2020-06
Date: 2020-07-21 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developersen els darrers dos mesos:

  * Richard Laager (rlaager)
  * Thiago Andrade Marques (andrade)
  * Vincent Prat (vivi)
  * Michael Robin Crusoe (crusoe)
  * Jordan Justen (jljusten)
  * Anuradha Weeraman (anuradha)
  * Bernelle Verster (indiebio)
  * Gabriel F. T. Gomes (gabriel)
  * Kurt Kremitzki (kkremitzki)
  * Nicolas Mora (babelouest)
  * Birger Schacht (birger)
  * Sudip Mukherjee (sudip)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Marco Trevisan
  * Dennis Braun
  * Stephane Neveu
  * Seunghun Han
  * Alexander Johan Georg Kjäll
  * Friedrich Beckmann
  * Diego M. Rodriguez
  * Nilesh Patra
  * Hiroshi Yokota

Enhorabona a tots!

