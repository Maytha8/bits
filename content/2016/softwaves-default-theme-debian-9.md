Title: "softWaves" will be the default theme for Debian 9
Slug: softwaves-will-be-the-default-theme-for-debian-9
Date: 2016-10-25 19:50
Author: Laura Arjona Reina and Niels Thykier
Tags: stretch, artwork
Status: published

The theme ["softWaves"][softwaves-link]
by Juliette Taka Belin has been selected
as default theme for Debian 9 'stretch'.

[![softWaves Login screen. Click to see the whole theme proposal](|static|/images/softwaves_login.png)][softwaves-link]

[softwaves-link]: https://wiki.debian.org/DebianArt/Themes/softWaves

After the Debian Desktop Team made the
[call for proposing themes](https://lists.debian.org/debian-desktop/2016/06/msg00001.html),
a total of [twelve choices](https://wiki.debian.org/DebianDesktop/Artwork/Stretch)
have been submitted, and any Debian contributor has received
the opportunity to vote on them in a survey.
We received 3,479 responses ranking the different choices,
and softWaves has been the winner among them.

We'd like to thank all the designers that have participated
providing nice wallpapers and artwork for Debian 9,
and encourage everybody interested in this area of Debian,
to join the [Design Team](https://wiki.debian.org/Design).
It is being [considered](https://lists.debian.org/debian-desktop/2016/09/msg00002.html)
to package all of them so they are easily available in Debian.
If you want to help in this effort, or package any other artwork
(for example, particularly designed to be accessibility-friendly),
please contact the [Debian Desktop Team](mailto:debian-desktop@lists.debian.org), but hurry up,
because the freeze for new packages in the next release of Debian
starts on January 5th, 2017.

This is the second time that Debian ships a theme by Juliette Belin,
who also created the theme ["Lines"](https://wiki.debian.org/DebianArt/Themes/Lines)
that enhances our actual stable release, Debian 8.
Congratulations, Juliette, and thank you very much for your continued commitment to Debian!
