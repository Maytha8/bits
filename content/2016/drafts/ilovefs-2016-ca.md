Title: I love Free Software Day 2016: Mostreu el vostre amor pel programari lliure
Slug: ilovefs-2016
Date: 2016-02-14 00:00
Author: Laura Arjona Reina
Translator: Adrià García-Alzórriz
Lang: ca
Tags: contributing, debian, free software, FSFE
Status: draft

[![ILoveFS banner](|static|/images/ilovefs-banner-medium-en.png)][ilovefs-link]

Avui 14 de febrer, la Fundació Europea del Programari Lliure (FSFE) 
celebra el [Dia «I Love Free Software»][ilovefs-link]. El dia «I Love 
Free Software» és aquell dia per tal que els usuaris del programari 
lliure apreciïn i agraeixin els col·laboradors de les seves aplicacions
de programari preferides, projectes i entitats.

Aprofitem l'oportunitat per dir "gràcies" a tots aquells «upstreams» i 
«downstreams», desenvolupadors i col·laboradors de Debian. Gràcies per 
la vostra feina i dedicació envers el programari lliure!

[![ILoveFS banner](|static|/images/ilovefs-banner-medium-en.png)][ilovefs-link]

<!---
note for translators: try a localized banner, e.g. https://fsfe.org/campaigns/ilovefs/artwork/graphics/ilovefs-banner-medium-fr.png
-->

Hi ha moltes maneres de participar en aquest dia «ILoveFS» i encoratgem
a tothom per unir-s'hi i celebrar-lo. Mostreu el vostre entusiasme 
envers els desenvolupadors, col·laboradors i equips de Debian de forma 
virtual, a les xarxes socials usant el «hashtag» #ilovefs i difonent-ho
als vostres cercles, o bé visitant la pàgina web de la campanya 
[ILoveFS](http://ilovefs.org) on hi trobareu material promocional 
disponible per usar com ara postals i _banners_.

Per conèixer més sobre la FSFE, podeu llegir el seu [anunci per a la campanya][announcement-link]
o visitar la seva [pàgina web general][fsfe-link].

[ilovefs-link]: https://fsfe.org/campaigns/ilovefs/2016/
[announcement-link]: https://fsfe.org/news/2016/news-201608-01.html
[fsfe-link]: https://fsfe.org
