Title: Nya Debianutvecklare och Debian Maintainers (November och December 2023)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Alexandre Detiste (tchet)
  * Amin Bandali (bandali)
  * Jean-Pierre Giraud (jipege)
  * Timthy Pearson (tpearson)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Safir Secerovic

Grattis!

