Title: नए डेबियन डेवलपर्स और मेंटेनर्स (नवंबर और दिसंबर 2023)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: hi-IN
Translator: Yashraj Moghe
Status: published


पिछले दो महीनों में निम्नलिखित सहायकों को उनका डेबियन डेवलपर अकाउंट मिला:

  * Alexandre Detiste (tchet)
  * Amin Bandali (bandali)
  * Jean-Pierre Giraud (jipege)
  * Timthy Pearson (tpearson)

और निम्नलिखित को डेबियन मेंटेनर का स्थान दिया गया हैं:

  * Safir Secerovic

इन्हें बधाईयाँ!

