Title: Nuevos desarrolladores y mantenedores de Debian (noviembre y diciembre del 2023)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developer en los dos últimos meses:

  * Alexandre Detiste (tchet)
  * Amin Bandali (bandali)
  * Jean-Pierre Giraud (jipege)
  * Timthy Pearson (tpearson)

Lo siguiente colaborador del proyecto se convirtió en Debian Maintainers en los dos últimos meses:

  * Safir Secerovic

¡Felicidades a todos!

