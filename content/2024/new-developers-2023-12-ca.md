Title: Nous desenvolupadors i mantenidors de Debian (novembre i desembre del 2023)
Slug: new-developers-2023-12
Date: 2024-01-31 16:00
Author: Donald Norwood
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * Alexandre Detiste (tchet)
  * Amin Bandali (bandali)
  * Jean-Pierre Giraud (jipege)
  * Timthy Pearson (tpearson)

El següent col·laborador del projecte ha esdevingut Debian Maintainer en els darrers dos mesos:

  * Safir Secerovic

Enhorabona a tots!

