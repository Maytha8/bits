Title: Help the Java Team distribute your project!
Slug: help-the-java-team-distribute-your-project
Date: 2019-06-17 09:00
Author: Hans-Christoph Steiner
Tags: java
Status: draft

There is a vast array of great free software projects written in Java.
All sorts of large systems that we all rely on every day are built
upon the Apache Foundation libraries.  Large companies like Google and
IBM put out standard libraries that so many other projects use.
Unfortunately, the standard practice for distributing Java code makes
it a lot of work to integrate them into Debian.

The Debian Java Team's work is generally under-appreciated, so we are
getting the word out here. The Java Team has to consistently fight the
Java standard practice of bundling all deps into a single JAR.  This
means there is no shared security updates, each dev has to update
every dependency themselves in that model.  That works great for large
companies with staff devoted to doing that.

For the majority of Debian use cases, that works poorly.  Debian
delivers on the promise that people can just `apt install foo` and
have it work, and receive security updates.  The user does not even
need to know what language the program is written in, it just works.

The Java developer community need to embrace the value of these use
cases, and help Debian by making it easier to package Java projects in
the standard distro method, with shared dependencies that are
independently updated.

Python and Ruby provide great examples of more flexible standard
practice for shipping software.  Both have methods of describing the
dependencies needed, and then automatically fetching them.  They are
designed in a way that is quite easy to hook into the native build
system and make Debian packages.  That is sadly not the case with
_Gradle_ and _Maven_, the most popular build systems for Java.  For
those, the Java Team usually has to extensively patch the build system
to make it work for the Debian package.

## How you can help

There are many things that upstream projects can do to make it a lot
easier to include them in Debian and other distros.

* Include a build target in your build system that builds using only
  libraries in Debian.
* Provide a minimal build target which uses as few build tricks as
  possible, like Gradle plugins or custom hacks.
* Run Continuous Integration (CI) builds against Debian/testing, its
  so easy these days with GitLab CI, Travis CI, etc.
* Help us package Java, and learn first hand!

For more information, you can see [@apo's talk at FOSDEM](https://fosdem.org/2019/schedule/event/debian_java/).  Questions are always welcome on [debian-java@lists.debian.org](https://lists.debian.org/debian-java/) and [#debian-java](irc://irc.oftc.net/debian-java).
