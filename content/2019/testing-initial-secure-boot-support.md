Title: Help test initial support for Secure Boot
Slug: testing-initial-secure-boot-support
Date: 2019-02-02 11:00
Author: Steve McIntyre, Cyril Brulebois
Tags: buster, installer
Status: published

The Debian Installer team is happy to report that the Buster Alpha 5
release of the installer includes some **initial** support for UEFI
Secure Boot (SB) in Debian's installation media.

**This support is not yet complete**, and we would like to request
some help! Please read on for more context and instructions to help us
get better coverage and support.


On amd64 machines, by default the Debian installer will now boot (and
install) a signed version of the `shim` package as the first stage
boot loader. Shim is the core package in a signed Linux boot chain on
Intel-compatible PCs. It is responsible for validating signatures on
further pieces of the boot process (GRUB and the Linux kernel),
allowing for verification of those pieces. Each of those pieces will
be signed by a Debian *production* signing key that is baked into the
`shim` binary itself.

However, for safety during the development phase of Debian's SB
support, we have only been using a temporary test key to sign our GRUB
and Linux packages. If we made a mistake with key management or trust
path verification during this development, this would save us from
having to revoke the production key. We plan on switching to the
production key soon.

Due to the use of the test key so far, out of the box Debian will
**not** yet install or run with SB enabled; Shim will not validate
signatures with the test key and will stop, reporting the
problem. This is correct and useful behaviour!

Thus far, Debian users have needed to disable SB before installation
to make things work. From now on, with SB still disabled, installation and
use should work just the same as previously. Shim simply chain-loads
GRUB and continues through the boot chain without checking signatures.

It is possible to enrol more keys on a SB system so that shim will
recognise and allow other signatures, and this is how we have been
able to test the rest of the boot chain. We now invite more users to
give us valuable test coverage on a wider variety of hardware by
enrolling our Debian test key and running with SB enabled.

**If you want to help us test our Secure Boot support**, please follow
the
[instructions in the Debian wiki](https://wiki.debian.org/SecureBoot/Testing)
and provide feedback.

With help from users, we expect to be able to ship fully-working and
tested UEFI Secure Boot in an upcoming Debian Installer release and in
the main Buster release itself.
