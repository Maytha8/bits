Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2019)
Slug: new-developers-2019-02
Date: 2019-03-12 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Paulo Henrique de Lima Santana (phls)
  * Unit 193 (unit193)
  * Marcio de Souza Oliveira (marciosouza)
  * Ross Vandegrift (rvandegrift)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Romain Perier
  * Felix Yan

Félicitations !

