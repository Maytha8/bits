Title: Infomaniak Patrocinador Platinum da DebConf19
Slug: infomaniak-platinum-debconf19
Date: 2019-02-21 15:45
Author: Laura Arjona Reina
Artist: Infomaniak
Tags: debconf19, debconf, sponsors, infomaniak
Lang: pt-BR
Translator: Sergio Durigan Junior
Image: /images/infomaniak.png
Status: published

[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com/)

Estamos muito felizes em anunciar que
a [**Infomaniak**](https://www.infomaniak.com/) comprometeu-se a
apoiar a [DebConf19](https://debconf19.debconf.org) como um
**patrocinador Platinum**.

*"A Infomaniak está orgulhosa de apoiar a Conferência anual dos(as)
Desenvolvedores(as) do Debian"*, disse Marc Oehler, *Chief Operating
Officer* da Infomaniak. *"A grande maioria do nosso trabalho de
hospedagem funciona utilizando o Debian, e nós compartilhamos os
valores dessa comunidade: promover inovação enquanto garantimos que a
segurança, transparência e liberdade de usuário continuem sendo
prioridades principais."*

A Infomaniak é a maior empresa de hospedagem web da Suíça, que também
oferece serviços de *backup* e de armazenamento, soluções para
organizadores de eventos, serviços de transmissão em tempo real e
vídeo sob demanda. Todos os *datacenters* e elementos críticos para o
funcionamento dos serviços e produtos oferecidos pela empresa (tanto
*software* quanto *hardware*) são de propriedade absoluta da
Infomaniak.

Com esse comprometimento como Patrocinador Platinum, a Infomaniak
contribui para tornar possível nossa conferência anual e apoia
diretamente o progresso do Debian e do Software Livre, ajudando a
fortalecer a comunidade, que continua a colaborar com projetos do
Debian durante o restante do ano.

Muito obrigado Infomaniak por apoiar a DebConf19!

## Torne-se um patrocinador também!

A DebConf19 ainda está aceitando patrocinadores. Empresas e
organizações interessadas devem entrar em contato com o time da
DebConf através
do [sponsors@debconf.org](mailto:sponsors@debconf.org), e visitar o
website da DebConf19
em [https://debconf19.debconf.org](https://debconf19.debconf.org).
