Title: Nous desenvolupadors i mantenidors de Debian (maig i juny del 2022)
Slug: new-developers-2022-06
Date: 2022-07-29 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * Geoffroy Berret (kaliko)
  * Arnaud Ferraris (aferraris)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Alec Leanas
  * Christopher Michael Obbard
  * Lance Lin
  * Stefan Kropp
  * Matteo Bini
  * Tino Didriksen

Enhorabona a tots!

