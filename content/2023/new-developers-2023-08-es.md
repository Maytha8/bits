Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Marius Gripsgard (mariogrip)
  * Mohammed Bilal (rmb)
  * Lukas Märdian (slyon)
  * Robin Gustafsson (rgson)
  * David da Silva Polverari (polverari)
  * Emmanuel Arias (eamanu)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Aymeric Agon-Rambosson
  * Blair Noctis
  * Lena Voytek
  * Philippe Coval
  * John Scott

¡Felicidades a todos!

