Title: Nous desenvolupadors i mantenidors de Debian (novembre i desembre del 2022)
Slug: new-developers-2022-12
Date: 2023-01-24 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * Dennis Braun (snd)
  * Raúl Benencia (rul)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Gioele Barabucci
  * Agathe Porte
  * Braulio Henrique Marques Souto
  * Matthias Geiger
  * Alper Nebi Yasak
  * Fabian Grünbichler
  * Lance Lin

Enhorabona a tots!

