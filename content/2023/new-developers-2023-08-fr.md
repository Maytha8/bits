Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2023)
Slug: new-developers-2023-08
Date: 2023-09-27 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Marius Gripsgard (mariogrip)
  * Mohammed Bilal (rmb)
  * Lukas Märdian (slyon)
  * Robin Gustafsson (rgson)
  * David da Silva Polverari (polverari)
  * Emmanuel Arias (eamanu)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Aymeric Agon-Rambosson
  * Blair Noctis
  * Lena Voytek
  * Philippe Coval
  * John Scott

Félicitations !

