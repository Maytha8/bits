Title: Nous desenvolupadors i mantenidors de Debian (setembre i octubre del 2023)
Slug: new-developers-2023-10
Date: 2023-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: published


Els següents col·laboradors del projecte han esdevingut Debian Developers en els darrers dos mesos:

  * François Mazen (mzf)
  * Andrew Ruthven (puck)
  * Christopher Obbard (obbardc)
  * Salvo Tomaselli (ltworf)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Bo YU
  * Athos Coimbra Ribeiro
  * Marc Leeman
  * Filip Strömbäck

Enhorabona a tots!

