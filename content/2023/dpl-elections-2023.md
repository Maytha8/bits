Title: Debian Project Leader Election 2023, Jonathan Carter re-elected
Slug: dpl-elections-2023
Date: 2023-04-24 21:00
Author: Donald Norwood
Tags: dpl
Status: published


The voting period for the Debian Project Leader election has ended, with all of the votes tallied we announce the winner is: Jonathan Carter, who has been elected for the forth time.

Congratulations! The new term for the project leader started on 2023-04-21.

279 of 997 Developers voted using the [Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).

More information about the results of the voting are available on the [Debian Project Leader Elections 2023](https://www.debian.org/vote/2023/suppl_001_stats) page. 

Many thanks all of our Developers for voting. 
