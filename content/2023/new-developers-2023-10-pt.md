Title: Novos(as) desenvolvedores(as) e mantenedores(as) Debian (setembro e outubro de 2023)
Slug: new-developers-2023-10
Date: 2023-11-30 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator: 
Status: published


Os(As) seguintes colaboradores(as) do projeto se tornaram Desenvolvedores(as) Debian nos últimos dois meses:

  * François Mazen (mzf)
  * Andrew Ruthven (puck)
  * Christopher Obbard (obbardc)
  * Salvo Tomaselli (ltworf)

Os(As) seguintes colaboradores(as) do projeto se tornaram Mantenedores(as) Debian nos últimos dois meses:

  * Bo YU
  * Athos Coimbra Ribeiro
  * Marc Leeman
  * Filip Strömbäck

Parabéns a todos!

