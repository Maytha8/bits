Title: Arduino de retour dans Debian
Slug: arduino-back-in-debian
Date: 2021-02-01 08:00
Author: Rock Storm
Lang: fr
Translator: Jean-Pierre Giraud
Tags: arduino, announce
Status: published

L'équipe Debian Electronics est fière d'annoncer que la dernière version
d'Arduino, la plateforme de programmation de microcontrôleurs AVR
probablement la plus répandue, est maintenant
[empaquetée et chargée dans Debian unstable][1].

La dernière version d'Arduino disponible dans Debian était la
version 1.0.5 qui datait de 2013. Il a fallu des années d'essais et d'échecs
pour finalement, après des mois d'efforts soutenus de Carsten Schoenert et
Rock Storm, pour obtenir un paquet qui fonctionne avec la dernière version
d'Arduino. Après plus de sept ans, les utilisateurs vont à nouveau pouvoir
installer facilement l'environnement de développement d'Arduino avec cette
seule commande : *"apt install arduino"*.

*"Le but de ce message n'est pas seulement d'annoncer cette nouvelle version
mais plus encore une demande de tests"* a déclaré Rock Storm. *" Le titre
aurait bien pu être : _RECHERCHÉS : bêta testeurs pour Arduino_ (morts ou
vifs :P)."* L'équipe Debian Electronics apprécierait que toutes celles et
tous ceux qui possèdent les outils et les connaissances nécessaires testent
le paquet et nous fassent savoir s'ils ou elles ont rencontré des
problèmes.

Avec ce message, nous souhaitons remercier l'équipe Debian Electronics et
tous les anciens contributeurs de ce paquet. Cette prouesse n'aurait pas
été possible sans eux.

[1]: https://packages.debian.org/unstable/arduino
