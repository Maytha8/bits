Title: Nuevos desarrolladores y mantenedores de Debian (julio y agosto del 2021)
Slug: new-developers-2021-08
Date: 2021-09-13 17:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: published


Los siguientes colaboradores del proyecto se convirtieron en Debian Developers en los dos últimos meses:

  * Aloïs Micard (creekorful)
  * Sophie Brun (sophieb)

Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Douglas Andrew Torrance
  * Marcel Fourné
  * Marcos Talau
  * Sebastian Geiger

¡Felicidades a todos!

