Title: Eleição 2021 do(a) líder do Projeto Debian 2021, Jonathan Carter reeleito.
Slug: 2021dpl-election
Date: 2021-04-18 10:00
Author: Donald Norwood
Tags: dpl
Lang: pt-BR
Translator: Paulo Henrique de Lima Santana (phls)
Status: published

O período de votação e a contagem de votos para a eleição do(a) líder do
Projeto Debian acabou de ser concluída, e o vencedor é Jonathan Carter!

455 de 1.018 desenvolvedores(as) votaram usando o 
[método Condorcet](http://en.wikipedia.org/wiki/Condorcet_method).

Mais informações sobre os resultados da votação são disponíveis na página
[eleições 2021 do(a) líder do Projeto Debian](https://www.debian.org/vote/2021/vote_001)

Muito obrigado ao Jonathan Carter e a Sruthi Chandra por suas campanhas, e
aos(as) nossos(as) desenvolvedores(as) pela votação.

