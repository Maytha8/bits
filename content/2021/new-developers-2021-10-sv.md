Title: Nya Debianutvecklare och Debian Maintainers (September och Oktober 2021)
Slug: new-developers-2021-10
Date: 2021-11-19 13:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: published


Följande bidragslämnare fick sina Debianutvecklarkonton under de senaste två månaderna:

  * Bastian Germann (bage)
  * Gürkan Myczko (tar)

Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Clay Stan
  * Daniel Milde
  * David da Silva Polverari
  * Sunday Cletus Nkwuda
  * Ma Aiguo
  * Sakirnth Nagarasa

Grattis!

