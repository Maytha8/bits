Title: Debsources 站点使用 sources.debian.org 域名上线
Date: 2017-12-13 18:40
Tags: mirror, announce, sources
Slug: debsources-now-in-sources-debian-org
Author:  Laura Arjona Reina
Status: published
Translator: Boyuan Yang
Lang: zh-CN

[**Debsources**](https://salsa.debian.org/qa/debsources)
是在互联网上发布、浏览与搜索未压缩 Debian 源码镜像的一个网页应用程序。
用户可以使用 [https://sources.debian.org](https://sources.debian.org)
的域名，经由 HTML 用户界面或其 [JSON API](https://sources.debian.org/doc/api/)
访问所有 Debian 发布版本中软件包的源代码。

该服务最初在2013年以 _sources.debian.net_ 的实例上线，由 [IRILL](http://www.irill.org)
友情提供托管。它现在成为了 **sources.debian.org** 的官方服务，在 Debian 的基础设施上托管。

该新实例提供旧有实例的全部功能（更新脚本每日运行四次，
附带各类插件，从以对代码行数、软件包大小进行计算到显示补丁和版权文件的子应用）
，以及与其它 Debian 服务，如 [codesearch.debian.net](https://codesearch.debian.net)
和 [PTS](http://packages.qa.debian.org/) 的集成。

Debsources 团队借助这次将实例迁移至 Debian 基础设施的机会正式地宣布了这个服务。
请阅读他们的[邮件](https://lists.debian.org/debian-devel-announce/2017/12/msg00000.html)以及
[Debsources 文档页面](https://sources.debian.org/doc/about/)以了解详情。
