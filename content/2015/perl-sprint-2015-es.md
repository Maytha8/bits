Title: Sprint de Debian Perl 2015
Slug: perl-sprint-2015
Date: 2015-07-13 21:00
Author: Alex Muntada
Tags: perl, sprint, barcelona
Status: published
Lang: es
Translator: Daniel Cialdella

El grupo de [Perl de Debian](https://wiki.debian.org/Teams/DebianPerlGroup)
tuvo su [primer sprint](https://wiki.debian.org/Sprints/2015/DebianPerlSprint) en
mayo y fue un éxito: 7 miembros se encontraron en Barcelona el fin de
semana del 22 al 24 de mayo para dar el puntapié inicial al desarrollo
en Perl para Stretch y para trabajar en tareas de control de
calidad en los más de 3000 paquetes que el grupo mantiene.

Aunque los participantes disfrutaron mucho de un clima agradable y de
la comida, se realizó también una gran cantidad de trabajo:

* 53 fallos fueron registrados o se trabajó sobre ellos, se
aceptaron 31 actualizaciones.
* Se discutió el proceso de gestión de parches (`quilt`) y se presentaron
alternativas posibles (`git-debcherry` y `git-dpm`).
* Se realizaron mejoras en las herramientas de Perl para Debian
(`dpt`) y se debatió cómo llevar el seguimiento de la historia del proyecto
original con Git y sus etiquetas.
* Se revisaron y actualizaron las políticas del grupo, la documentación
y tareas recurrentes.
* Se preparó la  versión Perl 5.22 y se debatieron los planes en
`src:perl` para Stretch.
* Se revisaron las listas blancas de `autopkgtest`, los nuevos paquetes
agregados y las notificaciones mediante IRC brindadas por KGB.
* Se revisaron también las migraciones pendientes.
* Se comentaron los problemas de reproducibilidad con `POD_MAN_DATE`.

Se publicó el [informe completo](https://lists.debian.org/debian-perl/2015/07/msg00009.html)
en las listas de correos de Debian relevantes.

Los participantes desean agradecer al
[Departamento de Arquitectura de Computadores](http://www.ac.upc.edu/)
de la Universidad Politécnica de Catalunya su alojamiento,
y a los donantes del Proyecto Debian que ayudaron a cubrir
parte de los gastos.
