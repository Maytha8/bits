Title:Debian.org enabled for SIP federation and WebRTC, XMPP/Jabber to follow
Date: 2014-01-28 17:45
Tags: sip, rtc, xmpp, jabber
Slug: debian-sip-webrtc
Author: Daniel Pocock
Status: published

[Debian System Administrators](https://dsa.debian.org/) working in conjunction with [pkg-voip team](http://pkg-voip.alioth.debian.org/)
member Daniel Pocock have set up a SIP proxy and TURN server for debian.org.

Specifically, the SIP proxy provides a way for Debian Developers to use
their Debian email ID as a SIP address for making calls to other project
members and exchanging calls with any other domain that is enabled for
SIP. The repro SIP proxy from [reSIProcate](http://www.resiprocate.org/Main_Page) has been chosen for this project.

The TURN server provides a mechanism for users of SIP or XMPP (Jabber)
to relay audio and video streams through a public IP address when
necessary, eliminating many of the quality issues that arise when NAT
devices block the media streams in one or both directions.

The service allows the users to connect directly with the SIP device or
softphone of their choosing, including many of those packaged in Debian
such as [Jitsi](https://jitsi.org/) and
[Empathy](https://wiki.gnome.org/action/show/Apps/Empathy) or third party solutions like
[Lumicall](http://www.lumicall.org/) or
[CSipSimple](http://code.google.com/p/csipsimple/) on Android.
The SIP proxy also includes a [WebRTC interface](https://rtc.debian.org),
allowing Debian Developers to immediately try [WebRTC](http://www.webrtc.org/) voice and video
calls without installing or configuring any software of their own other
than a web browser.

A second stage of the project involves providing an XMPP (Jabber) server
with similar capabilities for federated communications between
debian.org users and other domains. Further details will be announced
in the weeks ahead.

It is a significant feature of the Debian Project philosophy that we can
operate the entire project using free software, specifically, using
software available in Debian packages running on our own infrastructure
and without a dependency on third party cloud solutions.These new
services for project members fulfill those expectations. It is
particularly relevant for situations where real-time communication
(voice or video) collaboration takes place with third parties such as
applicants for [Google Summer of
Code](https://developers.google.com/open-source/soc/), [Outreach Program for
Women](https://wiki.gnome.org/OutreachProgramForWomen),
sponsors, media and other free software projects.

Project specific details and a user guide are available now on the
Debian Wiki at
[http://wiki.debian.org/UnifiedCommunications/DebianDevelopers](http://wiki.debian.org/UnifiedCommunications/DebianDevelopers)
