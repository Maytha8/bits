#!/usr/bin/env python3
# -*- coding: utf-8 -*- #

# Basic details
AUTHOR = 'The Debian Project'
SITENAME = 'Bits from Debian'
SITESUBTITLE = 'Blog from the Debian Project'
# This is only required in publishconf.py
SITEURL = ''
PATH = 'content'
OUTPUT_PATH = 'output/'

# Configuration
TIMEZONE = 'Europe/Paris'
DEFAULT_LANG = 'en'
DELETE_OUTPUT_DIRECTORY = True
THEME = "theme-bits"
DEFAULT_PAGINATION = 5
DISPLAY_PAGES_ON_MENU = True
SUMMARY_MAX_LENGTH = None
LOCALE='C'

# URL settings
# We might want this for publication
RELATIVE_URLS = True
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_LANG_URL = '{date:%Y}/{date:%m}/{slug}-{lang}.html'
ARTICLE_LANG_SAVE_AS = '{date:%Y}/{date:%m}/{slug}-{lang}.html'
AUTHOR_URL = ''
AUTHOR_SAVE_AS = ''

# Create month and year archives
YEAR_ARCHIVE_SAVE_AS = '{date:%Y}/index.html'
MONTH_ARCHIVE_SAVE_AS = '{date:%Y}/{date:%m}/index.html'

# Generate tag pages without pagination; otherwise, a high number of articles
# under tag debconf (for example) would eventually generate tag page
# debconf13.html, which would conflict with the actual debconf13 tag
PAGINATED_TEMPLATES = {'index': None, 'category': None, 'author': None}

# Feeds settings
FEED_ATOM = 'feeds/atom.xml'
FEED_RSS = 'feeds/feed.rss'
FEED_ALL_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
CATEGORY_FEED_ATOM = None
CATEGORY_FEED_RSS = None
TAG_FEED_ATOM = None
TAG_FEED_RSS = None
TRANSLATION_FEED = None
TRANSLATION_FEED_ATOM = 'feeds/atom-{lang}.xml'
TRANSLATION_FEED_RSS = 'feeds/feed-{lang}.rss'

# Do not create category pages
CATEGORIES_SAVE_AS = None
CATEGORY_SAVE_AS = ''
CATEFORY_URL = None

MENUITEMS =  ()

SOCIAL = (('Project News', 'https://www.debian.org/News/'),
          ('Micronews ','https://micronews.debian.org/'))


PATH = 'content'
STATIC_PATHS = [
    'extras/favicon.ico',
	'images',
    ]
EXTRA_PATH_METADATA = {
    'extras/favicon.ico': {'path': 'favicon.ico'},
    }

# Plugins

PLUGINS = ["add_translator_line", "add_artist_line"]
PLUGIN_PATHS = ["plugins"]
