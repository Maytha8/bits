# Makefile for updating bits.debian.org
# This makefile can also be used in local to recreate the blog.

PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=$(BASEDIR)/output
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py

BASEDIR_DPL=$(CURDIR)/dpl
OUTPUTDIR_DPL=$(OUTPUTDIR)/dpl

# Directory where the generated blog must be copied in dillon.debian.org
OUTPUTDIRMASTER=/srv/bits-master.debian.org/htdocs

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -v -D
endif


help:
	@echo 'Makefile for a pelican Web site                                                 '
	@echo '                                                                                '
	@echo 'Usage local:                                                                    '
	@echo '   make html                           (re)generate the web site                '
	@echo '   make html-full                      (re)generate the web site and subsites   '
	@echo '   make clean                          remove the generated files               '
	@echo '   make serve [PORT=8000]              serve site locally at http://localhost:8000'
	@echo '   make publish                        generate using production settings       '
	@echo 'Usage press team in dillon.debian.org:                                          '
	@echo '   make mclean                         remove the generated files in dillon.d.o '
	@echo '   make mpublish                       generate and push the blog post          '
	@echo '                                                                                '
	@echo '                                                                                '

######################################
# Rules to generate the blog locally #
######################################

html: clean
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
	touch $(OUTPUTDIR)/.gitkeep

html-full: html
	mkdir -p $(OUTPUTDIR_DPL)
	$(MAKE) -C $(BASEDIR_DPL) OUTPUTDIR=$(OUTPUTDIR_DPL) DEBUG=$(DEBUG) html

clean:
	find $(OUTPUTDIR) -mindepth 1 -delete
	touch $(OUTPUTDIR)/.gitkeep

regenerate:
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
	touch $(OUTPUTDIR)/.gitkeep

serve:
	$(PELICAN) -l

publish: clean
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)

#############################################
# Rules for publishing in dillon.debian.org #
#############################################

mclean:
	mkdir -p $(OUTPUTDIRMASTER)/dpl
	$(MAKE) -C $(BASEDIR_DPL) OUTPUTDIRMASTER=$(OUTPUTDIRMASTER)/dpl DEBUG=$(DEBUG) mclean
	find $(OUTPUTDIRMASTER) -mindepth 1 -delete

mpublish: mclean
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIRMASTER) -s $(PUBLISHCONF) $(PELICANOPTS)
	mkdir -p $(OUTPUTDIRMASTER)/dpl
	$(MAKE) -C $(BASEDIR_DPL) OUTPUTDIRMASTER=$(OUTPUTDIRMASTER)/dpl DEBUG=$(DEBUG) mpublish
	chmod -R g+w $(OUTPUTDIRMASTER)/*
	/usr/local/bin/static-update-component bits.debian.org
	@echo 'Published!'

.PHONY: html help clean serve publish mclean mpublish
